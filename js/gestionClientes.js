  var clientesObtenidos;

function getClientes(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest ();
  request.onreadystatechange = function(){
      if(this.readyState == 4 && this.status == 200){
        //console.log(request.responseText);

        clientesObtenidos = request.responseText;
        procesarClientes();

      }
  }
  request.open("GET", url, true);
  request.send();

}


function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONProductos.value[0].ProductName);

  var divClientes = document.getElementById("divClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";


  tabla.classList.add("table");
  tabla.classList.add("table-striped"); // pintar renglon en blanco

  for(var i = 0; i < JSONClientes.value.length ;i++){
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila     = document.createElement("tr");

    var columnacustomer  = document.createElement("td");
    columnacustomer.innerText = JSONClientes.value[i].ContactName;

    var columnaPais = document.createElement("td");
    columnaPais.innerText = JSONClientes.value[i].City;

    var columnaBanderaPais = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");

    if(JSONClientes.value[i].Country == "UK"){
      imgBandera.src = rutaBandera + "United-Kingdom.png";

    }else{
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png" ;

    }
    columnaBanderaPais.appendChild(imgBandera);

    nuevaFila.appendChild(columnacustomer);
    nuevaFila.appendChild(columnaPais);
    nuevaFila.appendChild(columnaBanderaPais);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divClientes.appendChild(tabla);

}
